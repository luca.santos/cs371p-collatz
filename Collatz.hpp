// ---------
// Collatz.h
// ---------

#ifndef Collatz_h
#define Collatz_h

// --------
// includes
// --------

#include <iostream> // istream, ostream
#include <tuple>    // tuple
#include <utility>  // pair

using namespace std;

// ------------
// collatz_read
// ------------

/**
 * read two ints
 * @param a string
 * @return a pair of ints
 */
pair<int, int> collatz_read (const string&);

// ------------
// collatz_eval
// ------------

/**
 * @param a pair of ints
 * @return a tuple of three ints
 */
tuple<int, int, int> collatz_eval (const pair<int, int>&);

/**
* solve max cycle length for specified range
* @param a pair of ints
* @return a single int
*/
int cycleSolve  (int i, int j);

/**
 * solve cycle length for a single int
 * @param a single int
 * @return a single int
*/
int collatzSolve (int i);

/**
 * solves cycle length for a given range provided that the meta cache can be used
 * @param three ints
 * @return a single int
 */
int mCacheSolve(int i, int j, int temp);

/**
 * Basic collatz loo`for a given range
 * @param two ints
 * @return a single int
 */
int collatzLoop (int i, int j);

/**
 * builds meta cash from mCash.txt
*/
void getMCash();

// -------------
// collatz_print
// -------------

/**
 * print three ints
 * @param an ostream
 * @param a tuple of three ints
 */
void collatz_print (ostream&, const tuple<int, int, int>&);

// -------------
// collatz_solve
// -------------

/**
 * @param an istream
 * @param an ostream
 */
void collatz_solve (istream&, ostream&);

#endif // Collatz_h
