// -----------
// Collatz.c++
// -----------
// confirmed success on broken tests
// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair
#include <fstream>  // reading from metaCash

#include "Collatz.hpp"
using namespace std;
int lCache[1000000];
int mCache[1000];
#define CACHE_RANGE 1000

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {

    assert(!s.empty());
    istringstream sin(s);
    int i, j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// collatz_eval
// ------------

tuple<int, int, int> collatz_eval (const pair<int, int>& p) {

    int i, j, m, n, sol;
    tie(i, j) = p;
    m = i;
    n = j;

    if (i > j) {
        m = j;
        n = i;
    }

    //ensures hackerRank constraints are met
    assert( (i > 0) && (j > 0) && (i < 1000000) &&  (j < 1000000));

    sol = cycleSolve(m,n);

    // solution has to, at a minimum, be 1
    assert (sol > 0);

    return make_tuple(i,j,sol);
}

//solve max cycle for a given range

int cycleSolve (int i, int j) {

    if (i == j)
        return collatzSolve(i);

    int thresh = (j/2) + 1;

    //optimize by only calculating upper half
    //of a given range whenever possible
    i = i < thresh? thresh: i;

    return collatzLoop(i,j);
}

//solves cycle length for a given int

int collatzSolve(int i) {

    int cLength = 1;
    unsigned long int k = i;

    while (k != 1) {

        if (k < 1000000 && lCache[k] > 0) {
            cLength += lCache[k] - 1;
            break;
        }

        if (k%2 == 0) {

            k /= 2;
        } else {

            //if k is odd, we can slightly optimize our code
            //by skipping a future cycle, since we know that
            //any odd number times 3 + 1 will result in an even
            //number; thus we end up incrementing our current
            //cycle length by two
            k = k + (k >> 1) + 1;
            cLength++;
        }

        cLength++;
    }
    lCache[i] = cLength;
    return cLength;
}

//solves the collatz problem for a given range provided that we
//know at least of one max cycle length range we can instantly retrieve
//from the meta cache
int mCacheSolve (int i, int j, int temp) {

    //by definition, this HAS to be at least a 1000; temp is, at this point
    //	the closest CACHE_RANGE threshold to i
    assert (temp > CACHE_RANGE);

    //the first index to retrieve and the return value
    int iStart = (temp/CACHE_RANGE) - 1, max = 0;

    //get max cycle length from i to next CACHE_RANGE boundary
    max = collatzLoop(i,temp);

    temp = (j-temp)/CACHE_RANGE;//determines how many total values we are able
    //to retireve from the meta cache

    //max meta cache index
    int lim = temp + iStart;

    for (iStart; iStart < lim; iStart++) {
        max = mCache[iStart] > max? mCache[iStart]: max;
    }

    temp = collatzLoop((iStart+1)*CACHE_RANGE,j);//determine leftover
    //max cycle lenght
    max = temp > max? temp: max;
    return max;
}

//calls collatzSolve for a given i,j range
int collatzLoop(int i, int j) {

    int max = 0, temp = 0;
    for (int l = i; l <= j; l++) {
        temp = collatzSolve(l);
        max = temp > max? temp: max;
    }
    return max;
}

//builds meta cash from txt file
void getMCash() {

    string s;

    int i = 0;

    //www.cplusplus.com/reference/strings/stoi/
    std :: string :: size_type t;

    ifstream file ("mCash.txt");
    if (file.is_open()) {

        while (getline(file,s)) {

            mCache[i] = std :: stoi (s, &t);
            ++i;
        }
    }
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& sout, const tuple<int, int, int>& t) {
    int i, j, v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& sin, ostream& sout) {
    string s;
    //getMCash();
    while (getline(sin, s))
        collatz_print(sout, collatz_eval(collatz_read(s)));
}
