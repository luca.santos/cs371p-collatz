#include <stdio.h>
int cycleSolve(int i, int j);
int collatzSolve(int i);
int valCache[1000000];
void createFile(FILE * fp);

int main()
{
    FILE *fp;
    fp = fopen("mCash.txt","w");
    createFile(fp);

    return 0;
}


void createFile(FILE * fp) {
    
    int i = 1, j = 1, l = 1, num;
    char num_str[10];

    for (l; l <= 1000; l++) {
        
        j = (l * 1000);
        i = j - 1000;
        num = cycleSolve(i,j);
        // printf("i = %d, j = %d, num = %d \n", i, j, num);
        sprintf(num_str, "%d\n", num);
       	fputs(num_str,fp);
    }
    fclose(fp);
}

int cycleSolve(int i,int j) {
    int max = 0, temp = 0;
    i = i == 0? 1: i;
    
    for (int l = i; l <=j; l++) {
        temp = collatzSolve(l);
        valCache[l] = temp;
        max = temp > max? temp: max;
    }
    return max;
}

int collatzSolve(int i) {
    int cLength = 1, orig = i;
    unsigned int k = i;
    //printf ("n is %d \n", i);
    while (k > 1) {
        if (k <= 999999 && valCache[k] > 0) {
            cLength += valCache[k] - 1;
            break;
        } else {
            if (k%2 == 0) {
                k /= 2;
            	cLength++;
            } else {
                k = k + (k >> 1) + 1;
		cLength+=2;
                //assert (i != 1);
            }
            
            //printf("n is %llu, and cLength is %d \n", k, cLength);   
        }
    }
    //printf ("cycle length for %d is %d \n", orig, cLength);
    return cLength;
}
