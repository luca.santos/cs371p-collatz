// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----
// read
// ----

TEST(CollatzFixture, read) {
    ASSERT_EQ(collatz_read("1 10\n"), make_pair(1, 10));
}

// ----
// eval
// ----

TEST(CollatzFixture, eval0) {
    ASSERT_EQ(collatz_eval(make_pair(1, 10)), make_tuple(1, 10, 20));
}

TEST(CollatzFixture, eval1) {
    ASSERT_EQ(collatz_eval(make_pair(100, 200)), make_tuple(100, 200, 125));
}

TEST(CollatzFixture, eval2) {
    ASSERT_EQ(collatz_eval(make_pair(201, 210)), make_tuple(201, 210, 89));
}

TEST(CollatzFixture, eval3) {
    ASSERT_EQ(collatz_eval(make_pair(900, 1000)), make_tuple(900, 1000, 174));
}

TEST(CollatzFixture, eval4) {
    ASSERT_EQ(collatz_eval(make_pair(1000, 900)), make_tuple(1000, 900, 174));
}

TEST(CollatzFixture, eval5) {
    ASSERT_EQ(collatz_eval(make_pair(210, 201)), make_tuple(210, 201, 89));
}

// ----
// cycleSolve
// ----

TEST(CollatzFixture, cSol0) {
    ASSERT_NE(cycleSolve (50,50), 1);
}

TEST(CollatzFixture, cSol1) {
    ASSERT_EQ(cycleSolve(50,50), 25);
}

TEST(CollatzFixture, cSol2) {
    ASSERT_EQ (cycleSolve(1,1), 1);
}

TEST(CollatzFixture, cSol3) {
    ASSERT_EQ (cycleSolve(1,0), 0);
}



// ----
// collatzSolve
// ----

TEST(CollatzFixture, coSol0) {
    ASSERT_EQ(collatzSolve(123), 47);
}

TEST(CollatzFixture, coSol1) {
    ASSERT_EQ(collatzSolve(500), 111);
}

TEST(CollatzFixture, coSol2) {
    ASSERT_EQ(collatzSolve(1),1);
}

TEST(CollatzFixture, coSol3) {
    ASSERT_EQ(collatzSolve(379),59);
}

TEST(CollatzFixture, coSol4) {
    ASSERT_EQ(collatzSolve(999999),259);
}

TEST(CollatzFixture, coSol5) {
    ASSERT_EQ(collatzSolve(2),2);
}

TEST(CollatzFixture, coSol6) {
    ASSERT_EQ(collatzSolve(65103),74);
}

TEST(CollatzFixture, coSol7) {
    ASSERT_EQ(collatzSolve(1785),30);
}

// ----
// mCacheSolve, now obsolete
// ----

// ----
// collatzLoop
// ----

TEST(CollatxFixture, coLo0) {
    ASSERT_EQ(collatzLoop(194, 6361),262);
}

// ----
// getMCash, now obsolete
// ----

// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream sout;
    collatz_print(sout, make_tuple(1, 10, 20));
    ASSERT_EQ(sout.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream sin("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n");
}
